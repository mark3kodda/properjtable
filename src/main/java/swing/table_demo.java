package swing;


import models.Person;
import services.dataservices.filesservices.JSONFileServices;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class table_demo {

    private JFrame frame;
    private JTextField id;
    private JTextField name;
    private JTextField cont;
    private JTextField course;
    private JTable table;
    private JScrollPane scrollPane;
    private JButton btnDelete;
    private JButton btnUpdate;
    private JButton btnClear;

    private String[] dbList = {"-choose DB-","mySQL","PostgreSQL","MongoDB","Reddis","Cassandra","H2","GraphDB","JSON"};
    private JComboBox btnRead = new JComboBox(dbList);

    private List<Person> listOfPerson = new ArrayList<>();                      // в принципе список один на всё приложение - должен лишь перезаливаться - обновляться

    File jsonFileSource = new File("jsonFile.json");        // путь на файл                            // оба эти момент должны работать через фабрику
    JSONFileServices jsonFS = new JSONFileServices(jsonFileSource);   // реализация интерфейса чтение запись


    DefaultTableModel model;



    public JFrame getFrame() {
        return frame;
    }

    public table_demo() {
        initialize();
    }

    private void initialize() {

        frame = new JFrame("Super-PersonManager-App-3000");
        frame.setBounds(100, 100, 539, 437);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JPanel panel = new JPanel();
        panel.setBackground(new Color(176, 196, 222));
        panel.setBounds(0, 0, 531, 410);
        frame.getContentPane().add(panel);
        panel.setLayout(null);


        JLabel lblName = new JLabel("ID:");
        lblName.setBounds(21, 83, 46, 14);
        panel.add(lblName);

        JLabel lblName_1 = new JLabel("Name:");
        lblName_1.setBounds(21, 106, 46, 14);
        panel.add(lblName_1);

        JLabel lblContact = new JLabel("Contact:");
        lblContact.setBounds(21, 127, 46, 14);
        panel.add(lblContact);

        JLabel lblCourse = new JLabel("Course");
        lblCourse.setBounds(21, 148, 46, 14);
        panel.add(lblCourse);

        id = new JTextField();
        id.setBounds(67, 81, 132, 17);
        id.setColumns(10);
        panel.add(id);


        name = new JTextField();
        name.setColumns(10);
        name.setBounds(67, 102, 132, 17);
        panel.add(name);

        cont = new JTextField();
        cont.setColumns(10);
        cont.setBounds(67, 124, 132, 17);
        panel.add(cont);

        course = new JTextField();
        course.setColumns(10);
        course.setBounds(67, 145, 132, 17);
        panel.add(course);

        scrollPane = new JScrollPane();
        scrollPane.setBounds(224, 48, 297, 339);
        panel.add(scrollPane);

        table = new JTable();
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                int i = table.getSelectedRow();
                id.setText(model.getValueAt(i, 0).toString());
                name.setText(model.getValueAt(i, 1).toString());
                cont.setText(model.getValueAt(i, 2).toString());
                course.setText(model.getValueAt(i, 3).toString());

                listOfPerson.set(i,
                        new Person(Integer.parseInt(id.getText()), name.getText(),
                                cont.getText(),course.getText()));


                jsonFS.writeFile(listOfPerson); // запись в файл
            }
        });

        table.setBackground(new Color(240,248,255));
        model = new DefaultTableModel();
        Object[] column = {"ID","Name","Contact","Course"};
        final Object[] row = new Object[4];

        //тут происходит чтение

        model.setColumnIdentifiers(column);
        table.setModel(model);

        scrollPane.setViewportView(table);

        //----------------- кнопка Add

        JButton btnAdd = new JButton("Add");
        btnAdd.addActionListener(e -> {

            if(id.getText().equals("")|| name.getText().equals("")||
                    cont.getText().equals("") || course.getText().equals(""))
            {
                JOptionPane.showMessageDialog(null,"Please Fill Complete Info");
            }else {
                row[0] = id.getText();
                row[1] = name.getText();
                row[2] = cont.getText();
                row[3] = course.getText();
                model.addRow(row); // а возможно ли через этот метод залить сюда лист???

                listOfPerson.add(new Person(Integer.parseInt(id.getText()),name.getText(),cont.getText(),course.getText()));
                jsonFS.writeFile(listOfPerson);

                id.setText("");
                name.setText("");
                cont.setText("");
                course.setText("");
                JOptionPane.showMessageDialog(null,"Saved Successfully");
                /*
                System.out.println(listOfPerson);

                 */

            }
        });

        btnRead.setBounds(21,236,180,23);
        panel.add(btnRead);
        btnRead.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(e.getSource() == btnRead){

                    System.out.println(btnRead.getSelectedItem());

//                    System.out.println(e.getSource());
                    //передзаливкой инфы на стол должно происходить очищение

                    if(btnRead.getSelectedItem().equals("JSON")){ // эта штука должна происходить по фабрике

                        listOfPerson = jsonFS.readFile();
                        for (int i = 0; i <listOfPerson.size() ; i++) {
                            row[0] = listOfPerson.get(i).getId();
                            row[1] = listOfPerson.get(i).getName();
                            row[2] = listOfPerson.get(i).getCont();
                            row[3] = listOfPerson.get(i).getCourse();
                            model.addRow(row);
                        }
                        System.out.println(listOfPerson.toString());
                    }
                }
            }
        });

        //думай за метод который будет сетать лист в рабочее поле



//        public void addRowToJTable(List<Person> list)
//        {
//            String rowData[][] = new String[list.size()][5];
//            for (int i = 0; i < list.size() ; i++) {
//                rowData[i][0] = String.valueOf(list.get(i).getId());
//                rowData[i][1] = list.get(i).getFname();
//                rowData[i][2] = list.get(i).getLname();
//                rowData[i][3] = String.valueOf(list.get(i).getAge());
//                rowData[i][4] = list.get(i).getAddress();
//                model.addRow(rowData[i]);
//            }
//        }



        btnAdd.setBounds(21,280,80,23);
        panel.add(btnAdd);

        btnDelete = new JButton("Delete");
        btnDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int i = table.getSelectedRow();

                if(i>=0){
                    model.removeRow(i);
                    JOptionPane.showMessageDialog(null,"Deleted Successfully");

                    listOfPerson.remove(i);

                    jsonFS.writeFile(listOfPerson);
                }
                else
                {
                    JOptionPane.showMessageDialog(null,"Please Select a Row First");
                }


            }
        });

        btnDelete.setBounds(21,324,80,23);
        panel.add(btnDelete);

        btnUpdate = new JButton("Update");
        btnUpdate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int i = table.getSelectedRow();

                if(i >= 0) {
                    model.setValueAt(id.getText(), i, 0);
                    model.setValueAt(name.getText(), i, 1);
                    model.setValueAt(cont.getText(), i, 2);
                    model.setValueAt(course.getText(), i, 3);
                    JOptionPane.showMessageDialog(null,"Updated Successfully");
                    listOfPerson.set(i, new Person(Integer.parseInt(id.getText()),name.getText(),cont.getText(),course.getText()));
                    jsonFS.writeFile(listOfPerson);

                    //упаковка и отправка

//                    System.out.println("list updated");
//                    System.out.println(listOfPerson);



                }
                else
                {
                    JOptionPane.showMessageDialog(null,"Please Select a Row First");
                }
            }
        });
        btnUpdate.setBounds(123,280,80,23);
        panel.add(btnUpdate);

        btnClear = new JButton("Clear");
        btnClear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                id.setText("");
                name.setText("");
                cont.setText("");
                course.setText("");



//                row[0] = id.getText();
//                row[1] = name.getText();
//                row[2] = cont.getText();
//                row[3] = course.getText();
//                model.addRow(row);
                System.out.println(listOfPerson.size());

                model.setRowCount(0);


                System.out.println(listOfPerson.toString());
            }
        });
        btnClear.setBounds(123,324,80,23);
        panel.add(btnClear);



    }
}
