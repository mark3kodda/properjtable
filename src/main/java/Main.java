import swing.table_demo;
import java.awt.*;

public class Main {

    public static void main(String[] args) {

        EventQueue.invokeLater(() -> {
            try {
                table_demo window = new table_demo();
                window.getFrame().setVisible(true);
            } catch (Exception e){
                e.printStackTrace();
            }
        });

    }

}
