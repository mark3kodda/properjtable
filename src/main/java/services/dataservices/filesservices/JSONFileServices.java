package services.dataservices.filesservices;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.Person;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JSONFileServices{

    private File file;

    public JSONFileServices(File file) {
        this.file = file;
    }
    private ObjectMapper mapper = new ObjectMapper();

    public List<Person> readFile() {
        if(file.length() == 0){
            return null;
        }


        Person[] readList;
        try {
            readList = mapper.readValue(file, Person[].class);

        } catch (IOException e) {
            System.out.println("Problem with read in json ");
            e.printStackTrace();
            return null;
        }
        List<Person> personList = new ArrayList<Person>(Arrays.asList(readList));
        return personList;
    }

    public void writeFile(List<Person> listPerson) {
        try {
            Person[] people = listPerson.toArray(new Person[0]);
            mapper.writeValue(file, listPerson);
        } catch (IOException e) {
            System.out.println("Problem with write in json ");
            e.printStackTrace();
        }
    }
}
